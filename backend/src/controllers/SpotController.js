const Spot = require('../models/Spot')
const User = require('../models/User')


module.exports = {
 
    async index(req, res){
        const {tech} = req.query
        const spots = await Spot.find({techs: tech})
        return res.json(spots)
    },
 
    async store(req, res){
       const {company, price,techs} = req.body
       const {user_id} = req.headers
       const {filename} = req.file

       const user = await User.findById(user_id)
       if(!user){
           return res.json({
               status: false,
               message: 'User not found in system'
           })
       }

        const spot = await Spot.create({
            thumbnail: filename,
            company,
            price,
            techs: techs.split(',').map( resp => resp.trim() ),
            user: user_id
        })  

      return res.json(spot)
    }
}