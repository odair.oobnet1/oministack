const User = require('../models/User')

module.exports = {
    async find(req, res){
        const {email} = req.body
        let user = await User.findOne({email})
        if(!user){
            return res.status(400).json({
                status:false,
                message: 'User not exist'
            })
        }
        
        return res.json(user)
    },

   async store(req, res){
        const {email} = req.body
        let user = await User.findOne({email})
        if(!user){
            user = await User.create({email})
        }
        return res.json(user)
    }
}