const express = require('express')
const route = express.Router();
const multer = require('multer')
const multerConfig = require('./config/multer')

const SessionController = require('./controllers/SessionController')
const SpotController = require('./controllers/SpotController')
const DashController = require('./controllers/DashController')
const BookingController = require('./controllers/BookingController')
const upload = multer(multerConfig)

route.post('/sessions',SessionController.find)
route.post('/users',SessionController.store)
route.get('/spots', SpotController.index)
route.post('/spots',upload.single('thumbnail'), SpotController.store)

route.get('/dash', DashController.show)
route.post('/spots/:spot_id/bookings', BookingController.store)
module.exports = route;