// require('dotenv').config()
// require('dotenv-extended').load();

const express = require('express')
const path = require('path');
const socket = require('socket.io')
const http = require('http')
const cors  = require('cors')
const morgan  = require('morgan')
const mongoose = require('mongoose')

const app = express()
const server = http.Server(app);
const io = socket(server)
const port = 3022
const routes = require('./routes')
const baseUrlConnect = 'mongodb+srv://user:pass@cluster0-pyozz.mongodb.net/test?retryWrites=true&w=majority'
mongoose.connect(baseUrlConnect, { useNewUrlParser:true,useUnifiedTopology:true }, function(error, succ) {
    if (error) return console.log(error);
    
    console.log('connect mongodb');
})

const connectUsers = {}
io.on('connection', clientSokect => {
    const {user_id} = clientSokect.handshake.query
    connectUsers[user_id] = clientSokect.id
})
app.use( (req, res, next) =>{
    req.io = io
    req.connectUsers = connectUsers
    next()
})

app.use( morgan('dev') )
app.use( cors() )
app.use( express.json() )
// require('./config/global')(global)
app.use('/files', express.static(path.resolve(__dirname,'..','uploads')))
app.use(routes)

server.listen(port, () => console.log(`Example app listening on port port!`))